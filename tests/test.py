import logging
import os
import unittest
from pathlib import Path
from calrissian.report import initialize_reporter, write_report, CPUParser, MemoryParser
from calrissian.executor import ThreadPoolJobExecutor
from calrissian.main import (
    install_tees,
    add_arguments,
    install_signal_handler,
    flush_tees,
    activate_logging,
)
from calrissian.report import initialize_reporter, write_report, CPUParser, MemoryParser
from calrissian.context import CalrissianLoadingContext, CalrissianRuntimeContext
from cwltool.main import main as cwlmain
from cwltool.argparser import arg_parser
from calrissian.version import version
from calrissian.k8s import delete_pods

from nose2.tools import params

class TestCalrissianJob(unittest.TestCase):
    @classmethod
    def setUpClass(cls):

        cls.level = logging.INFO

        cls.parser = arg_parser()
        add_arguments(cls.parser)
        cls.parser.add_argument("--post_stac_item", type=str)
        cls.parser.add_argument("--pre_stac_item", type=str)
        cls.parser.add_argument("--aoi", type=str)

    @params(
        (
            "https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs/items/S2B_53HPA_20210723_0_L2A",
            "https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs/items/S2B_53HPA_20210703_0_L2A",
            "136.659,-35.96,136.923,-35.791",
        )
    )
    def test_aoi(self, pre, post, aoi):

        parsed_args = self.parser.parse_args(
            [
                "--max-ram",
                "16G",
                "--max-cores",
                "8",
                "--tmp-outdir-prefix",
                "/calrissian-tmp/",
                "--outdir",
                "/calrissian-output/",
                "app-package.cwl#dnbr",
                "--post_stac_item",
                pre,
                "--pre_stac_item",
                post,
                "--aoi",
                aoi,
            ]
        )

        out = "/tmp/log_aoi.out"
        err = "/tmp/log.err"
        max_ram = "8G"
        max_cores = "16"
        max_ram_megabytes = MemoryParser.parse_to_megabytes(max_ram)
        max_cores = CPUParser.parse(max_cores)
        executor = ThreadPoolJobExecutor(max_ram_megabytes, max_cores)
        install_tees(out, err)
        #activate_logging(self.level)
        initialize_reporter(max_ram_megabytes, max_cores)

        runtime_context = CalrissianRuntimeContext(vars(parsed_args))
        runtime_context.select_resources = executor.select_resources

        install_signal_handler()
        try:

            result = cwlmain(
                args=parsed_args,
                executor=executor,
                loadingContext=CalrissianLoadingContext(),
                runtimeContext=runtime_context,
                versionfunc=version,
                logger_handler=logging.FileHandler(
                    "spam.log"
                ),  # logging.StreamHandler(), # logging.FileHandler('spam.log'), #logging.getLogger('calrissian.main').addHandler(logging.StreamHandler()),
            )
        finally:
            # Always clean up after cwlmain
            delete_pods()
            if parsed_args.usage_report:
                write_report(parsed_args.usage_report)
            flush_tees()

        import json

        with open(out) as f:
            d = json.load(f)

        is_dnbr = Path(os.path.join(d["stac"]["path"], "dnbr.tif")).is_file()
        is_dnbr1 = Path(os.path.join(d["stac"]["path"], "dnbr.tif")).is_file()

        os.remove(out)
        assert all([is_dnbr, is_dnbr1]) 

    @params(
        (
            "https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs/items/S2B_53HPA_20210723_0_L2A",
            "https://earth-search.aws.element84.com/v0/collections/sentinel-s2-l2a-cogs/items/S2B_53HPA_20210703_0_L2A",
        )
    )
    def test_no_aoi(self, pre, post):

        parsed_args = self.parser.parse_args(
            [
                "--max-ram",
                "16G",
                "--max-cores",
                "8",
                "--tmp-outdir-prefix",
                "/calrissian-tmp/",
                "--outdir",
                "/calrissian-output/",
                "app-package.cwl#dnbr",
                "--post_stac_item",
                pre,
                "--pre_stac_item",
                post,
            ]
        )

        out = "log.out"
        err = "log.err"
        max_ram = "8G"
        max_cores = "16"
        max_ram_megabytes = MemoryParser.parse_to_megabytes(max_ram)
        max_cores = CPUParser.parse(max_cores)
        executor = ThreadPoolJobExecutor(max_ram_megabytes, max_cores)
        install_tees(out, err)
        activate_logging(self.level)
        initialize_reporter(max_ram_megabytes, max_cores)

        runtime_context = CalrissianRuntimeContext(vars(parsed_args))
        runtime_context.select_resources = executor.select_resources

        install_signal_handler()
        try:

            result = cwlmain(
                args=parsed_args,
                executor=executor,
                loadingContext=CalrissianLoadingContext(),
                runtimeContext=runtime_context,
                versionfunc=version,
                logger_handler=logging.FileHandler(
                    "spam.log"
                ),  # logging.StreamHandler(), # logging.FileHandler('spam.log'), #logging.getLogger('calrissian.main').addHandler(logging.StreamHandler()),
            )
        finally:
            # Always clean up after cwlmain
            delete_pods()
            if parsed_args.usage_report:
                write_report(parsed_args.usage_report)
            flush_tees()
        print(result)
        assert result == 2

if __name__ == "__main__":
    import nose2

    nose2.main()
