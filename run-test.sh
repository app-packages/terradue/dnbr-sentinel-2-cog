#!/bin/sh

set -e

kubectl create -f job-test.yaml
kubectl wait --for=condition=Ready --selector=job-name=job-app-unit-test pods
kubectl logs -f jobs/app-unit-test 